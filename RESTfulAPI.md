# RESTful API

## replicas scale

1. scale pods
```shell
# update pods count
curl -X PATCH -H "Authorization: Bearer sha256~qvsFqBVYgyAIy3AObfLC3A-UceKTq041JUhSVH5Y53I" -H "Content-Type: application/json-patch+json" -k https://api.sandbox-m3.1530.p1.openshiftapps.com:6443/apis/apps/v1/namespaces/jumhorn-dev/deployments/web/scale --data '[{"op": "replace", "path": "/spec/replicas", "value": 2}]'
```

2. check pods status
```shell
# get pod status
curl -X GET -H "Authorization: Bearer sha256~qvsFqBVYgyAIy3AObfLC3A-UceKTq041JUhSVH5Y53I" -k https://api.sandbox-m3.1530.p1.openshiftapps.com:6443/apis/apps/v1/namespaces/jumhorn-dev/deployments/web/scale
```

3. full change pods info
```shell
# set pod status with json in file
curl -X PUT -H "Authorization: Bearer sha256~qvsFqBVYgyAIy3AObfLC3A-UceKTq041JUhSVH5Y53I" -H "Content-Type: application/json" -k https://api.sandbox-m3.1530.p1.openshiftapps.com:6443/apis/apps/v1/namespaces/jumhorn-dev/deployments/web/scale -d '@data.json'
```