# Use the official Debian image as the base image
FROM debian:stable-slim

# Set non-root user and group
ARG UID=1013140000
ARG GID=0

USER root

# install tools
RUN apt update
RUN apt install -y --no-install-recommends nginx git npm python3 python3-redis fcgiwrap curl
RUN npm install -g hexo-cli

# install debug tools
RUN apt install -y net-tools vim

# copy config file to container
COPY nginx.conf /etc/nginx/nginx.conf
COPY services.conf /etc/nginx/conf.d/default.conf

# create home
RUN mkdir -p /home/admin
# clone repos
RUN git clone https://github.com/JumHorn/www.git /home/admin/www
RUN git clone https://github.com/JumHorn/node-todo.git /home/admin/node-todo
RUN git clone https://github.com/JumHorn/node-redis-doodles.git /home/admin/node-redis-doodles
RUN git clone https://github.com/JumHorn/node-express-passport-mysql.git /home/admin/node-express-passport-mysql
RUN git clone https://github.com/JumHorn/jumhorn.github.io.git /home/admin/hexo

# install dependancies
COPY ttyd.x86_64 /home/admin/ttyd.x86_64

COPY entrypoint.sh /home/admin/entrypoint.sh

RUN cd /home/admin/node-todo && npm install
RUN cd /home/admin/node-redis-doodles && npm install
RUN cd /home/admin/node-express-passport-mysql && npm install
RUN cd /home/admin/hexo && npm install && hexo g

# add permissions
RUN chmod +x /home/admin/ttyd.x86_64
RUN chmod +x /home/admin/entrypoint.sh
RUN chmod +x /home/admin/www/api/*

# Create directories for Nginx logs and website files
RUN mkdir -p /var/log/nginx  && \
	mkdir -p /var/lib/nginx && \
	chown -R ${UID}:${GID} /home/admin && \
	chown -R ${UID}:${GID} /var/log/nginx /var/lib/nginx /var/run && \
	chmod -R ug+rwX /home/admin && \
	chmod -R ug+rwX /var/lib/nginx /var/log/nginx /var/run


# Expose port 8080
EXPOSE 8080

# Start Nginx when the container launches
ENTRYPOINT ["/home/admin/entrypoint.sh"]