#!/bin/bash

# copy passwd to repos
cp /mnt/mysql /home/admin/node-express-passport-mysql/config/database.js
cp /mnt/redis /home/admin/node-redis-doodles/.env
cp /mnt/mongodb /home/admin/node-todo/config/database.js

# Run nginx
nginx -g "daemon off;" &

# Run services
cd /home/admin/node-todo && node server.js &
cd /home/admin/node-redis-doodles && node server.js &
cd /home/admin/node-express-passport-mysql && node server.js &
cd /home/admin/hexo && hexo server -p 54001 --ip 127.0.0.1 &

# Run fcgiwrap
fcgiwrap -s unix:/var/run/fcgiwrap.socket &

# Run ttyd(has bugs if run in the background)
cd /home/admin
./ttyd.x86_64 -W -i lo bash